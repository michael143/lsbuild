import os
from lsbuild import lsbuild

rootdir = '.'
FN_NMR = []
for subdir, dirs, files in os.walk(rootdir):
    for fn_nmr in files:
        if not fn_nmr.endswith('.nmr'):
            continue
        fn_nmr = os.path.join(subdir, fn_nmr)
        FN_NMR.append({'path': fn_nmr, 'size': os.stat(fn_nmr).st_size})


FN_NMR = sorted(FN_NMR, key=lambda u: u['size'])
for fn_nmr in FN_NMR:
    lsbuild(fn_nmr['path'])
