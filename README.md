# LSBUILD

LSBUID is an app to solve Molecular Distance Geometry Problems (MDGP's) with inaccurate data.

## References

Souza, Michael, et al. "Solving the molecular distance geometry problem with inaccurate distance data." BMC bioinformatics 14.9 (2013): 1-6.
